<?php

return [
    'title'       => 'Swiper',
    'name'        => 'Swiper Field Type',
    'description' => 'Slideshow field type.',
];
