<?php namespace Defr\SwiperFieldType;

use Anomaly\RelationshipFieldType\Table\ValueTableBuilder;
use Anomaly\Streams\Platform\Addon\FieldType\FieldType;
use Defr\SwiperModule\Slideshow\Contract\SlideshowInterface;
use Defr\SwiperModule\Slideshow\SlideshowModel;
use Illuminate\Contracts\Cache\Repository;

class SwiperFieldType extends FieldType
{

    /**
     * The underlying database column type
     *
     * @var string
     */
    protected $columnType = 'integer';

    /**
     * The input view.
     *
     * @var string
     */
    // protected $inputView = 'defr.field_type.swiper::input';

    /**
     * The field type config.
     *
     * @var array
     */
    protected $config = [
        'mode' => 'default',
    ];

    /**
     * The cache repository.
     *
     * @var Repository
     */
    protected $cache;

    /**
     * Create a new FileFieldType instance.
     *
     * @param Repository $cache
     */
    public function __construct(Repository $cache)
    {
        $this->cache = $cache;
    }

    /**
     * Get the relation.
     *
     * @return BelongsTo
     */
    public function getRelation()
    {
        $entry = $this->getEntry();

        return $entry->belongsTo(
            array_get($this->config, 'related', SlideshowModel::class),
            $this->getColumnName()
        );
    }

    /**
     * Get the config.
     *
     * @return array
     */
    public function getConfig()
    {
        $config = parent::getConfig();

        return $config;
    }

    /**
     * Get the database column name.
     *
     * @return null|string
     */
    public function getColumnName()
    {
        return parent::getColumnName() . '_id';
    }

    /**
     * Return the config key.
     *
     * @return string
     */
    public function configKey()
    {
        $key = md5(json_encode($this->getConfig()));

        $this->cache->put('swiper-field_type::' . $key, $this->getConfig(), 30);

        return $key;
    }

    /**
     * Value table.
     *
     * @return string
     */
    public function valueTable()
    {
        $table = app(ValueTableBuilder::class);

        $slideshow = $this->getValue();

        if ($slideshow instanceof SlideshowInterface)
        {
            $slideshow = $slideshow->getId();
        }

        return $table->setSelected($slideshow)->build()->load()->getTableContent();
    }
}
