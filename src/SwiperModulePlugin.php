<?php namespace Defr\SwiperModule;

use Anomaly\Streams\Platform\Addon\Plugin\Plugin;
use Anomaly\Streams\Platform\Addon\Plugin\PluginCriteria;
use Anomaly\Streams\Platform\Support\Collection;
use Anomaly\Streams\Platform\Support\Decorator;
use Defr\SwiperModule\Slideshow\Command\GetSlideshow;

class SwiperModulePlugin extends Plugin
{

    /**
     * Return plugin functions.
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'slideshow',
                function ($slideshow = null, $config = [])
                {
                    $entry = $this->dispatch(new GetSlideshow($slideshow));

                    $entry->mergeConfig($config);

                    return (new Decorator())->decorate($entry);
                }
            ),
        ];
    }
}
