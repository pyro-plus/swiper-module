<?php namespace Defr\SwiperModule\Slide;

use Anomaly\Streams\Platform\Model\Swiper\SwiperSlidesEntryModel;
use Defr\SwiperModule\Slide\Contract\SlideInterface;

class SlideModel extends SwiperSlidesEntryModel implements SlideInterface
{

    /**
     * Gets the slideshow.
     *
     * @return SlideshowInterface The slideshow.
     */
    public function getSlideshow()
    {
        return $this->slideshow;
    }

    /**
     * Gets the image.
     *
     * @return FileInterface The image.
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Gets the name.
     *
     * @return string The name.
     */
    public function getName()
    {
        return $this->name;
    }

    public function getThumb()
    {
        return $this->image->url();
    }
}
