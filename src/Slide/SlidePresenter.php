<?php namespace Defr\SwiperModule\Slide;

use Anomaly\Streams\Platform\Entry\EntryPresenter;

class SlidePresenter extends EntryPresenter
{

    /**
     * Gets the image.
     *
     * @return string The image.
     */
    public function getThumb()
    {
        if ($image = $this->object->getImage())
        {
            return "<img
                src=\"{$image->getUrl()}\"
                alt=\"{$this->object->getName()}\"
            />";
        }

        return '';
    }
}
