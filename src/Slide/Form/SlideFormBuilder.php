<?php namespace Defr\SwiperModule\Slide\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class SlideFormBuilder extends FormBuilder
{

    /**
     * Fields to skip.
     *
     * @var array|string
     */
    protected $skips = [
        'slideshow',
    ];

    /**
     * The slideshow entry
     *
     * @var SlideshowInterface
     */
    protected $slideshow;

    /**
     * Gets the slideshow.
     *
     * @return SlideshowInterface The slideshow.
     */
    public function getSlideshow()
    {
        return $this->slideshow;
    }

    /**
     * Sets the slideshow.
     *
     * @param  SlideshowInterface $slideshow The slideshow
     * @return $this
     */
    public function setSlideshow($slideshow)
    {
        $this->slideshow = $slideshow;

        return $this;
    }

    /**
     * Fires on save form
     */
    public function onPost()
    {
        $this->getFormEntry()->slideshow_id = $this->slideshow->getId();
    }
}
