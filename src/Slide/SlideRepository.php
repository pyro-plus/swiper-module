<?php namespace Defr\SwiperModule\Slide;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\SwiperModule\Slideshow\Contract\SlideshowInterface;
use Defr\SwiperModule\Slide\Contract\SlideRepositoryInterface;

class SlideRepository extends EntryRepository implements SlideRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var SlideModel
     */
    protected $model;

    /**
     * Create a new SlideRepository instance.
     *
     * @param SlideModel $model
     */
    public function __construct(SlideModel $model)
    {
        $this->model = $model;
    }

    /**
     * Find all slides by slideshow
     *
     * @param  SlideshowInterface $slideshow
     * @return SlideCollection
     */
    public function findAllBySlideshow(SlideshowInterface $slideshow)
    {
        return $this->model->where('slideshow_id', $slideshow->getId())->get();
    }

    /**
     * Find slide by slug
     *
     * @param  string           $slug
     * @return SlideInterface
     */
    public function findBySlug(string $slug)
    {
        return $this->model->where('slug', $slug)->first();
    }
}
