<?php namespace Defr\SwiperModule\Slide\Table;

use Defr\SwiperModule\Slide\Contract\SlideRepositoryInterface;

class SlideTableEntries
{

    /**
     * @var mixed
     */
    protected $slides;

    /**
     * Create new instance of SlideTableEntries class
     *
     * @param SlideRepositoryInterface $slides
     */
    public function __construct(SlideRepositoryInterface $slides)
    {
        $this->slides = $slides;
    }

    /**
     * Handle the command
     *
     * @param SlideTableBuilder $builder The builder
     */
    public function handle(SlideTableBuilder $builder)
    {
        $builder->getTable()->setEntries($this->slides->findAllBySlideshow($builder->getSlideshow()));
    }

}
