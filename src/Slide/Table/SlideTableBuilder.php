<?php namespace Defr\SwiperModule\Slide\Table;

use Anomaly\Streams\Platform\Ui\Table\TableBuilder;

class SlideTableBuilder extends TableBuilder
{

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $columns = [
        'name',
        'image' => [
            'value' => 'entry.get_thumb',
        ],
    ];

    /**
     * The table buttons.
     *
     * @var array|string
     */
    protected $buttons = [
        'edit',
    ];

    /**
     * The table actions.
     *
     * @var array|string
     */
    protected $actions = [
        'delete',
    ];

    /**
     * Slideshow entry
     *
     * @var SlideshowInterface
     */
    protected $slideshow;

    /**
     * Gets the slideshow.
     *
     * @return SlideshowInterface The slideshow.
     */
    public function getSlideshow()
    {
        return $this->slideshow;
    }

    /**
     * Sets the slideshow.
     *
     * @param  SlideshowInterface $slideshow The slideshow
     * @return $this
     */
    public function setSlideshow($slideshow)
    {
        $this->slideshow = $slideshow;

        return $this;
    }

}
