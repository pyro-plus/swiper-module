<?php namespace Defr\SwiperModule\Slide\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;
use Defr\SwiperModule\Slideshow\Contract\SlideshowInterface;

interface SlideRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Find all slides by slideshow
     *
     * @param  SlideshowInterface $slideshow
     * @return SlideCollection
     */
    public function findAllBySlideshow(SlideshowInterface $slideshow);
}
