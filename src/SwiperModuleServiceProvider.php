<?php namespace Defr\SwiperModule;

use Anomaly\Streams\Platform\Addon\AddonCollection;
use Anomaly\Streams\Platform\Addon\AddonIntegrator;
use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Anomaly\Streams\Platform\Model\Repeater\RepeaterConfigsEntryModel;
use Anomaly\Streams\Platform\Model\Swiper\SwiperSlidesEntryModel;
use Anomaly\Streams\Platform\Model\Swiper\SwiperSlideshowsEntryModel;
use Defr\SwiperModule\Config\ConfigModel;
use Defr\SwiperModule\Config\ConfigRepository;
use Defr\SwiperModule\Config\Contract\ConfigRepositoryInterface;
use Defr\SwiperModule\Slideshow\Contract\SlideshowRepositoryInterface;
use Defr\SwiperModule\Slideshow\SlideshowModel;
use Defr\SwiperModule\Slideshow\SlideshowRepository;
use Defr\SwiperModule\Slide\Contract\SlideRepositoryInterface;
use Defr\SwiperModule\Slide\SlideModel;
use Defr\SwiperModule\Slide\SlideRepository;
use Defr\SwiperModule\SwiperModulePlugin;

class SwiperModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Addon plugins
     *
     * @var array
     */
    protected $plugins = [
        SwiperModulePlugin::class,
    ];

    /**
     * Addon routes
     *
     * @var array
     */
    protected $routes = [
        'admin/swiper'                              => 'Defr\SwiperModule\Http\Controller\Admin\SlideshowsController@index',
        'admin/swiper/choose'                       => 'Defr\SwiperModule\Http\Controller\Admin\SlideshowsController@choose',
        'admin/swiper/create'                       => 'Defr\SwiperModule\Http\Controller\Admin\SlideshowsController@create',
        'admin/swiper/edit/{id}'                    => 'Defr\SwiperModule\Http\Controller\Admin\SlideshowsController@edit',
        'admin/swiper/slides/{slideshow}'           => 'Defr\SwiperModule\Http\Controller\Admin\SlidesController@index',
        'admin/swiper/slides/{slideshow}/create'    => 'Defr\SwiperModule\Http\Controller\Admin\SlidesController@create',
        'admin/swiper/slides/{slideshow}/edit/{id}' => 'Defr\SwiperModule\Http\Controller\Admin\SlidesController@edit',
    ];

    /**
     * Addon bindings
     *
     * @var array
     */
    protected $bindings = [
        SwiperSlidesEntryModel::class     => SlideModel::class,
        RepeaterConfigsEntryModel::class  => ConfigModel::class,
        SwiperSlideshowsEntryModel::class => SlideshowModel::class,
    ];

    /**
     * Addon singletons
     *
     * @var array
     */
    protected $singletons = [
        SlideRepositoryInterface::class     => SlideRepository::class,
        ConfigRepositoryInterface::class    => ConfigRepository::class,
        SlideshowRepositoryInterface::class => SlideshowRepository::class,
    ];

    /**
     * Register the addon.
     *
     * @param AddonIntegrator $integrator
     * @param AddonCollection $addons
     */
    public function register(
        AddonIntegrator $integrator,
        AddonCollection $addons
    )
    {
        $addon = $integrator->register(
            realpath(__DIR__ . '/../addons/defr/swiper-field_type/'),
            'defr.field_type.swiper',
            true,
            true
        );

        $addons->push($addon);
    }

}
