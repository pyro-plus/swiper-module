<?php namespace Defr\SwiperModule\Slideshow\Form;

use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class SlideshowFormBuilder extends FormBuilder
{

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [
        'styles.css' => [
            'defr.module.swiper::scss/swiper.scss',
        ],
    ];

}
