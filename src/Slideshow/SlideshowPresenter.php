<?php namespace Defr\SwiperModule\Slideshow;

use Anomaly\Streams\Platform\Entry\EntryPresenter;
use Defr\SwiperModule\Slide\Contract\SlideInterface;

class SlideshowPresenter extends EntryPresenter
{

    /**
     * Get slides count
     *
     * @return integer
     */
    public function slidesCount()
    {
        return $this->object->getSlides()->count();
    }

    /**
     * Get thumbnails of slides of slideshow
     *
     * @return string
     */
    public function getThumbnails()
    {
        return $this->object->getSlides()->map(
            function (SlideInterface $slide) use ($width)
            {
                return "<img
                    src='{$slide->image->url}'
                    alt='{$slide->getName()}'
                    width='40px'
                />";
            }
        )->implode('');
    }

    /**
     * Render the slideshow
     *
     * @return Response
     */
    public function render(array $config = [])
    {
        $this->object->mergeConfig($config);

        return view('defr.module.swiper::swiper', ['slideshow' => $this]);
    }
}
