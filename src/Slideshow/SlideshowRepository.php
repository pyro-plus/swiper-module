<?php namespace Defr\SwiperModule\Slideshow;

use Anomaly\Streams\Platform\Entry\EntryRepository;
use Defr\SwiperModule\Slideshow\Contract\SlideshowRepositoryInterface;

class SlideshowRepository extends EntryRepository implements SlideshowRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var SlideshowModel
     */
    protected $model;

    /**
     * Create a new SlideshowRepository instance.
     *
     * @param SlideshowModel $model
     */
    public function __construct(SlideshowModel $model)
    {
        $this->model = $model;
    }

    /**
     * Finds slideshow by its slug
     *
     * @param  string               $slug The slug
     * @return SlideshowInterface
     */
    public function findBySlug(string $slug)
    {
        return $this->model->where('slug', $slug)->first();
    }

    /**
     * Finds slideshow by its slide
     *
     * @param  SlideInterface       $slide The slide
     * @return SlideshowInterface
     */
    public function findBySlide(SlideInterface $slide)
    {
        return $slide->getSlideshow();
    }
}
