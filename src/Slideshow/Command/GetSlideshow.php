<?php namespace Defr\SwiperModule\Slideshow\Command;

use Anomaly\Streams\Platform\Support\Collection;
use Defr\SwiperModule\Slideshow\Contract\SlideshowRepositoryInterface;

class GetSlideshow
{

    /**
     * Identifier
     *
     * @var mixed
     */
    protected $identifier;

    /**
     * Create an instance of GetSlideshow command
     *
     * @param mixed $identifier The identifier
     */
    public function __construct($identifier)
    {
        $this->identifier = $identifier;
    }

    /**
     * Handle the command
     *
     * @param  SlideshowRepositoryInterface $slideshows The slideshows
     * @return mixed
     */
    public function handle(SlideshowRepositoryInterface $slideshows)
    {
        $slideshow = $this->identifier;

        if ($slideshow instanceof Collection)
        {
            $slideshow = $this->identifier->get('slideshow');
        }

        if (is_array($slideshow))
        {
            $slideshow = array_get(
                $this->identifier,
                'slideshow',
                array_get(
                    $this->identifier,
                    'slug',
                    array_get($this->identifier, 'id')
                )
            );
        }

        if (is_integer($slideshow))
        {
            $slideshow = $slideshows->find($slideshow);
        }

        if (is_string($slideshow))
        {
            $slideshow = $slideshows->findBySlug($slideshow);
        }

        return $slideshow;
    }
}
