<?php namespace Defr\SwiperModule\Slideshow\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface SlideshowRepositoryInterface extends EntryRepositoryInterface
{

    /**
     * Finds slideshow by its slug
     *
     * @param  string               $slug The slug
     * @return SlideshowInterface
     */
    public function findBySlug(string $slug);
}
