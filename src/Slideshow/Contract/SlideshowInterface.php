<?php namespace Defr\SwiperModule\Slideshow\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryInterface;

interface SlideshowInterface extends EntryInterface
{

    /**
     * Gets the identifier.
     *
     * @return mixed The identifier.
     */
    public function getId();

    /**
     * Slides relation
     *
     * @return HasMany
     */
    public function slides();

    /**
     * Gets the slides.
     *
     * @return SlideCollection The slides.
     */
    public function getSlides();

    /**
     * Gets the configuration.
     *
     * @return array The configuration.
     */
    public function getConfig();

    /**
     * Merge configuration
     *
     * @param array $config The configuration
     */
    public function mergeConfig(array $config);
}
