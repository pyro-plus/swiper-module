<?php namespace Defr\SwiperModule\Slideshow;

use Anomaly\Streams\Platform\Model\Swiper\SwiperSlideshowsEntryModel;
use Defr\SwiperModule\Slideshow\Contract\SlideshowInterface;
use Defr\SwiperModule\Slide\SlideModel;

class SlideshowModel extends SwiperSlideshowsEntryModel implements SlideshowInterface
{

    /**
     * Slides
     *
     * @var SlideCollection
     */
    protected $slides;

    /**
     * Gets the identifier.
     *
     * @return mixed The identifier.
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Slides relation
     *
     * @return HasMany
     */
    public function slides()
    {
        return $this->hasMany(SlideModel::class, 'slideshow_id');
    }

    /**
     * Gets the slides.
     *
     * @return SlideCollection The slides.
     */
    public function getSlides()
    {
        return $this->slides()->get();
    }

    /**
     * Gets the configuration.
     *
     * @return array The configuration.
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Merge configuration
     *
     * @param array $config The configuration
     */
    public function mergeConfig(array $config)
    {
        $this->config = collect(array_merge($this->config->toArray(), $config));

        return $this;
    }
}
