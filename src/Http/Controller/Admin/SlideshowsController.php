<?php namespace Defr\SwiperModule\Http\Controller\Admin;

use Defr\SwiperModule\Slideshow\Form\SlideshowFormBuilder;
use Defr\SwiperModule\Slideshow\Table\SlideshowTableBuilder;
use Defr\SwiperModule\Slideshow\Contract\SlideshowRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class SlideshowsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param SlideshowTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(SlideshowTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Return the modal for choosing a slideshow.
     *
     * @param  SlideshowRepositoryInterface $slideshows
     * @return \Illuminate\View\View
     */
    public function choose(SlideshowRepositoryInterface $slideshows)
    {
        return view(
            'module::ajax/choose_slideshow',
            [
                'slideshows' => $slideshows->all(),
            ]
        );
    }

    /**
     * Create a new entry.
     *
     * @param SlideshowFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(SlideshowFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param SlideshowFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(SlideshowFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
