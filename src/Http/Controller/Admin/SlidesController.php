<?php namespace Defr\SwiperModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\SwiperModule\Slideshow\Contract\SlideshowRepositoryInterface;
use Defr\SwiperModule\Slide\Form\SlideFormBuilder;
use Defr\SwiperModule\Slide\Table\SlideTableBuilder;

class SlidesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param  SlideTableBuilder            $table
     * @param  SlideshowRepositoryInterface $slideshows The slideshows
     * @param  SlideshowRepositoryInterface $slideshow  The slideshow
     * @return Response
     */
    public function index(
        SlideTableBuilder $table,
        SlideshowRepositoryInterface $slideshows,
        $slideshow = null
    )
    {
        if (!$slideshow)
        {
            $this->messages->warning('Please choose a slideshow first.');

            return $this->response->redirectTo('admin/swiper');
        }

        $table->setSlideshow($slideshows->findBySlug($slideshow));

        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param  SlideFormBuilder $form
     * @return Response
     */
    public function create(
        SlideFormBuilder $form,
        SlideshowRepositoryInterface $slideshows,
        $slideshow
    )
    {
        if (!$slideshow)
        {
            $this->messages->warning('Please choose a slideshow first.');

            return $this->response->redirectTo('admin/swiper');
        }

        $form->setSlideshow($slideshows->findBySlug($slideshow));

        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param  SlideFormBuilder $form
     * @param  $id
     * @return Response
     */
    public function edit(SlideFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
