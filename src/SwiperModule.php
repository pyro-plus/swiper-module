<?php namespace Defr\SwiperModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class SwiperModule extends Module
{

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-sliders';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'slideshows' => [
            'buttons' => [
                'add',
            ],
        ],
        'slides'     => [
            'slug'        => 'slides',
            'data-toggle' => 'modal',
            'data-target' => '#modal',
            'data-href'   => 'admin/swiper/slides/{request.route.parameters.slideshow}',
            'href'        => 'admin/swiper/choose',
            'buttons'     => [
                'add',
            ],
        ],
    ];
}
