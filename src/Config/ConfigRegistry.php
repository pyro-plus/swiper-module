<?php namespace Defr\SwiperModule\Config;

class ConfigRegistry
{

    /**
     * The default configuration.
     *
     * @var array
     */
    protected $configs = [
        'default' => [
            'initialSlide'                  => 0,
            'direction'                     => 'horizontal',
            'speed'                         => 300,
            'setWrapperSize'                => false,
            'virtualTranslate'              => false,
            'width'                         => '',
            'height'                        => '',
            'autoHeight'                    => false,
            'roundLengths'                  => false,
            'nested'                        => false,
            'autoplay'                      => '-',
            'autoplayStopOnLast'            => false,
            'autoplayDisableOnInteraction'  => true,
            'watchSlidesProgress'           => false,
            'watchSlidesVisibility'         => false,
            'freeMode'                      => false,
            'freeModeMomentum'              => true,
            'freeModeMomentumRatio'         => 1,
            'freeModeMomentumVelocityRatio' => 1,
            'freeModeMomentumBounce'        => true,
            'freeModeMomentumBounceRatio'   => 1,
            'freeModeMinimumVelocity'       => 0.02,
            'freeModeSticky'                => false,
            'effect'                        => 'slide',
            'parallax'                      => false,
            'spaceBetween'                  => 0,
            'slidesPerView'                 => 1,
            'slidesPerColumn'               => 1,
            'slidesPerColumnFill'           => 'column',
            'slidesPerGroup'                => 1,
            'centeredSlides'                => false,
            'slidesOffsetBefore'            => 0,
            'slidesOffsetAfter'             => 0,
            'grabCursor'                    => false,
            'touchEventsTarget'             => 'container',
            'touchRatio'                    => 1,
            'touchAngle'                    => 45,
            'simulateTouch'                 => true,
            'shortSwipes'                   => true,
            'longSwipes'                    => true,
            'longSwipesRatio'               => 0.5,
            'longSwipesMs'                  => 300,
            'followFinger'                  => true,
            'onlyExternal'                  => false,
            'threshold'                     => 0,
            'touchMoveStopPropagation'      => true,
            'iOSEdgeSwipeDetection'         => false,
            'iOSEdgeSwipeThreshold'         => 20,
            'touchReleaseOnEdges'           => false,
            'passiveListeners'              => true,
            'resistance'                    => true,
            'resistanceRatio'               => 0.85,
            'preventClicks'                 => true,
            'preventClicksPropagation'      => true,
            'slideToClickedSlide'           => false,
            'allowSwipeToPrev'              => true,
            'allowSwipeToNext'              => true,
            'noSwiping'                     => true,
            'noSwipingClass'                => 'swiper-no-swiping',
            'swipeHandler'                  => null,
            'uniqueNavElements'             => true,
            'pagination'                    => null,
            'paginationType'                => 'bullets',
            'paginationHide'                => true,
            'paginationClickable'           => false,
            'paginationElement'             => 'span',
            'nextConfig'                    => null,
            'prevConfig'                    => null,
            'scrollbar'                     => null,
            'scrollbarHide'                 => true,
            'scrollbarDraggable'            => false,
            'scrollbarSnapOnRelease'        => false,
            'a11y'                          => false,
            'prevSlideMessage'              => 'Previous slide',
            'nextSlideMessage'              => 'Next slide',
            'firstSlideMessage'             => 'This is the first slide',
            'lastSlideMessage'              => 'This is the last slide',
            'paginationBulletMessage'       => 'Go to slide {{index}}',
            'keyboardControl'               => false,
            'mousewheelControl'             => false,
            'mousewheelForceToAxis'         => false,
            'mousewheelReleaseOnEdges'      => false,
            'mousewheelInvert'              => false,
            'mousewheelSensitivity'         => 1,
            'mousewheelEventsTarged'        => 'container',
            'hashnav'                       => false,
            'hashnavWatchState'             => false,
            'history'                       => '',
            'replaceState'                  => false,
            'preloadImages'                 => true,
            'updateOnImagesReady'           => true,
            'lazyLoading'                   => false,
            'lazyLoadingInPrevNext'         => false,
            'lazyLoadingInPrevNextAmount'   => 1,
            'lazyLoadingOnTransitionStart'  => false,
            'loop'                          => false,
            'loopAdditionalSlides'          => 0,
            'loopedSlides'                  => null,
            'zoom'                          => false,
            'zoomMax'                       => 3,
            'zoomMin'                       => 1,
            'zoomToggle'                    => true,
            'control'                       => 'undefined',
            'controlInverse'                => false,
            'controlBy'                     => 'slide',
            'normalizeSlideIndex'           => true,
            'observer'                      => false,
            'observeParents'                => false,
            'breakpoints'                   => '',
            'runCallbacksOnInit'            => true,
            'containerModifierClass'        => 'swiper-container-',
            'slideClass'                    => 'swiper-slide',
            'slideActiveClass'              => 'swiper-slide-active',
            'slideDuplicatedActiveClass'    => 'swiper-slide-duplicate-active',
            'slideVisibleClass'             => 'swiper-slide-visible',
            'slideDuplicateClass'           => 'swiper-slide-duplicate',
            'slideNextClass'                => 'swiper-slide-next',
            'slideDuplicatedNextClass'      => 'swiper-slide-duplicate-next',
            'slidePrevClass'                => 'swiper-slide-prev',
            'slideDuplicatedPrevClass'      => 'swiper-slide-duplicate-prev',
            'wrapperClass'                  => 'swiper-wrapper',
            'bulletClass'                   => 'swiper-pagination-bullet',
            'bulletActiveClass'             => 'swiper-pagination-bullet-active',
            'paginationHiddenClass'         => 'swiper-pagination-hidden',
            'paginationCurrentClass'        => 'swiper-pagination-current',
            'paginationTotalClass'          => 'swiper-pagination-total',
            'paginationProgressbarClass'    => 'swiper-pagination-progressbar',
            'paginationClickableClass'      => 'swiper-pagination-clickable',
            'paginationModifierClass'       => 'swiper-pagination-',
            'configDisabledClass'           => 'swiper-config-disabled',
            'lazyLoadingClass'              => 'swiper-lazy',
            'lazyStatusLoadingClass'        => 'swiper-lazy-loading',
            'lazyStatusLoadedClass'         => 'swiper-lazy-loaded',
            'lazyPreloaderClass'            => 'swiper-lazy-preloader',
            'preloaderClass'                => 'preloader',
            'zoomContainerClass'            => 'swiper-zoom-container',
            'notificationClass'             => 'swiper-notification',
        ],
    ];

    /**
     * Get a config.
     *
     * @param  $config
     * @return array|null
     */
    public function get($config)
    {
        if (!$config)
        {
            return null;
        }

        return array_get($this->configs, $config);
    }

    /**
     * Register a config.
     *
     * @param  $config
     * @param  array     $parameters
     * @return $this
     */
    public function register($config, array $parameters)
    {
        array_set($this->configs, $config, $parameters);

        return $this;
    }

    /**
     * Get the configs.
     *
     * @return array
     */
    public function getConfigs()
    {
        return $this->configs;
    }

    /**
     * Set the configs.
     *
     * @param  array   $configs
     * @return $this
     */
    public function setConfigs(array $configs)
    {
        $this->configs = $configs;

        return $this;
    }
}
