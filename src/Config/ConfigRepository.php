<?php namespace Defr\SwiperModule\Config;

use Defr\SwiperModule\Config\Contract\ConfigRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ConfigRepository extends EntryRepository implements ConfigRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ConfigModel
     */
    protected $model;

    /**
     * Create a new ConfigRepository instance.
     *
     * @param ConfigModel $model
     */
    public function __construct(ConfigModel $model)
    {
        $this->model = $model;
    }
}
