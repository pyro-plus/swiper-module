<?php namespace Defr\SwiperModule\Config\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ConfigRepositoryInterface extends EntryRepositoryInterface
{

}
