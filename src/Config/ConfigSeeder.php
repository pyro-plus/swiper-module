<?php namespace Defr\SwiperModule\Config;

use Anomaly\Streams\Platform\Database\Seeder\Seeder;
use Defr\SwiperModule\Config\ConfigRegistry;
use Defr\SwiperModule\Config\Contract\ConfigRepositoryInterface;

class ConfigSeeder extends Seeder
{

    /**
     * Config repository
     *
     * @var mixed
     */
    protected $repository;

    /**
     * Config registry
     *
     * @var mixed
     */
    protected $registry;

    /**
     * Create an instance of config seeder class
     *
     * @param ConfigRepository $repository
     * @param ConfigRegistry   $registry$registry;
     */
    public function __construct(ConfigRepositoryInterface $repository, ConfigRegistry $registry)
    {
        $this->repository = $repository;
        $this->registry   = $registry;
    }

    /**
     * Set the configs.
     *
     * @param  array   $configs
     * @return $this
     */
    public function run()
    {
        foreach ($this->registry->get('default') as $key => $value)
        {
            $this->repository->create([
                'key'   => $key,
                'value' => $value,
            ]);
        }
    }
}
