<?php

return [
    'name'      => [
        'name' => 'Name',
    ],
    'content'   => [
        'name' => 'Content',
    ],
    'slug'      => [
        'name' => 'Slug',
    ],
    'config'    => [
        'name' => 'Config',
    ],
    'slideshow' => [
        'name' => 'Slideshow',
    ],
    'slides'    => [
        'name' => 'Slides',
    ],
    'image'     => [
        'name' => 'Image',
    ],
    'key'       => [
        'name' => 'Key',
    ],
    'value'     => [
        'name' => 'Value',
    ],
];
