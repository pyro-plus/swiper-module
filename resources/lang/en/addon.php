<?php

return [
    'title'       => 'Swiper',
    'name'        => 'Swiper Module',
    'description' => '',
    'section'    => [
        'slideshows' => 'Slideshows',
        'slides'     => 'Slides',
    ],
];
