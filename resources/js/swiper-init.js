/* global Swiper */
/* eslint indent: [1, always] */

(function (document, window, Swiper) {
    var init = function () {
        var swipers = document.querySelectorAll('.swiper-container:not([data-initialized])');

        swipers.forEach(function (swiper) {
            var mySwiper = new Swiper(swiper, {

            });
            swiper.dataset.initialized = 'data-initialized';
        });
    };

    document.addEventListener('DOMContentLoaded', init)
})(document, window, Swiper);
