<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleSwiperCreateSlideshowsStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug'         => 'slideshows',
        'title_column' => 'name',
        'translatable' => true,
        'trashable'    => true,
        'sortable'     => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name'    => [
            'translatable' => true,
        ],
        'slug'    => [
            'required' => true,
            'unique'   => true,
        ],
        'content' => [
            'translatable' => true,
        ],
        'config',
        'image',
    ];

}
