<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Defr\SwiperModule\Config\ConfigModel;
use Defr\SwiperModule\Slideshow\SlideshowModel;

class DefrModuleSwiperCreateSwiperFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        'name'      => [
            'type' => 'anomaly.field_type.text',
        ],
        'slug'      => [
            'type'   => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type'    => '-',
            ],
        ],
        'content'   => [
            'type' => 'anomaly.field_type.wysiwyg',
        ],
        'config'    => [
            'locked' => false,
            'type'   => 'anomaly.field_type.repeater',
            'config' => [
                'related' => ConfigModel::class,
                'manage'  => false,
            ],
        ],
        'slideshow' => [
            'type'   => 'anomaly.field_type.relationship',
            'config' => [
                'related' => SlideshowModel::class,
                'mode'    => 'lookup',
            ],
        ],
        'image'     => [
            'type'   => 'anomaly.field_type.file',
            'config' => [
                'folders' => 'slides',
            ],
        ],
    ];

}
