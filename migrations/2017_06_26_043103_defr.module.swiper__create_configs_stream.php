<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleSwiperCreateConfigsStream extends Migration
{

    /**
     * The stream fields.
     *
     * @var array
     */
    protected $fields = [
        'key'        => [
            'locked'    => false,
            'namespace' => 'repeater',
            'type'      => 'anomaly.field_type.text',
        ],
        'value'  => [
            'locked'    => false,
            'namespace' => 'repeater',
            'type'      => 'anomaly.field_type.text',
        ],
    ];

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'namespace'    => 'repeater',
        'slug'         => 'configs',
        'title_column' => 'key',
        'sortable'     => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'key',
        'value',
    ];
}
